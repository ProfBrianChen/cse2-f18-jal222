//Jack Liu: CSE2 - Hw04
//9/23/18
//
//This program prints out the slang terminology of the output of a Craps game. Users can choose to either
//randomly generate dice being rolled or enter in specific values. This specific program will be using if-statements
//instead of switch statements.

import java.util.Scanner;

public class CrapsIf {
  
  public static void main(String args[]){
    
    Scanner myScanner = new Scanner( System.in ); //Setup new instance of the Scanner class called "myScanner"
    int diceOne; //Declaration of diceOne variable
    int diceTwo; //Declaration of diceTwo variable
    
    //Ask user if they want to manually enter the dice or get two random dice generated
    System.out.println("Would you like to manually enter the dice rolls? Respond with \"Y\" for yes OR \"N (or anything else) for no\" ");
    String diceResponse = myScanner.nextLine();
    
    
    //User wants to manually enter the two dice numbers
    if (diceResponse.equals("Y") || diceResponse.equals("y")){
        //Obtain dice #1's number
        System.out.println("Enter the roll of dice number 1: ");
        diceOne = myScanner.nextInt();

        //Obtain dice #2's number
        System.out.println("Enter the roll of dice number 2: ");
        diceTwo = myScanner.nextInt();
    }
    
    //User wants to have two randomly generated dice
    else {
      //Generate two random dice based on math random function
      diceOne = (int) (Math.random() * 6 + 1);
      diceTwo = (int) (Math.random() * 6 + 1);
      //Print out the rolled dice
      System.out.println("The dice that were rolled are: " + diceOne + " and " + diceTwo);
    }
    
    //Check if both dice have integer values between 1 and 6 inclusive.
    if (diceOne <= 6 && diceOne >= 1 && diceTwo <= 6 && diceTwo >= 1 ){
      
      //Set diceSum variable to the sum of diceOne and diceTwo to make it easier to find the combination w/o repeating code for order of the dice rolled
      int diceSum = diceOne + diceTwo;
      
      
      //Start Testing for sum combinations
      //In the case of a sum having two or more combinations, run a nested if-else statement to determine which slang term to use
      //Example. Sum of 4 has rolls of 2 and 2 OR 1 and 3
      
      if (diceSum == 2) {
        System.out.println("The slang for this roll is: Snake Eyes");
      }
      
      else if (diceSum == 3) {
        System.out.println("The slang for this roll is: Ace Deuce");
        
      }
      
      else if (diceSum == 4) {
        if (diceOne == 2 && diceTwo == 2){
          System.out.println("The slang for this roll is: Hard Four");
        }
        else {
          System.out.println("The slang for this roll is: Easy Four");
        }
      }       
      
      else if (diceSum == 5) {
        System.out.println("The slang for this roll is: Fever Five");
        
      }
      
      else if (diceSum == 6) {
        if (diceOne == 3 && diceTwo == 3){
          System.out.println("The slang for this roll is: Hard Six");
        }
        else {
          System.out.println("The slang for this roll is: Easy Six");
        }        
        
      }      
      else if (diceSum == 7) {
        System.out.println("The slang for this roll is: Seven Out");
      }
      
      else if (diceSum == 8) {
        if (diceOne == 4 && diceTwo == 4){
          System.out.println("The slang for this roll is: Hard Eight");
        }
        else {
          System.out.println("The slang for this roll is: Easy Eight");
        }            
      }
      else if (diceSum == 9) {
        System.out.println("The slang for this roll is: Nine");
      }
      
      else if (diceSum == 10) {
        if (diceOne == 5 && diceTwo == 5){
          System.out.println("The slang for this roll is: Hard Ten");
        }
        else {
          System.out.println("The slang for this roll is: Easy Ten");
        }            
      }
      
      else if (diceSum == 11) {
        System.out.println("The slang for this roll is: Yo-leven");
      }
      else if (diceSum == 12) {
        System.out.println("The slang for this roll is: Boxcars");
      }
      
      
    }
    
    //One or more dice is out of the range
    else {
      System.out.println("One of the specified dice roll's is out of range (1-6)");
    }
  
    
  }
  
}