//Jack Liu: CSE2 - Hw04
//9/23/18
//
//This program prints out the slang terminology of the output of a Craps game. Users can choose to either
//randomly generate dice being rolled or enter in specific values. This specific program will be using switch statements
//instead of if-else statements.

import java.util.Scanner;

public class CrapsSwitch {
  
  public static void main(String args[]){
    
    Scanner myScanner = new Scanner( System.in ); //Setup new instance of the Scanner class called "myScanner"
    int diceOne; //Declaration of diceOne variable
    int diceTwo; //Declaration of diceTwo variable
    
    //Ask user if they want to manually enter the dice or get two random dice generated
    System.out.println("Would you like to manually enter the dice rolls? Respond with \"Y\" for yes OR \"N (or anything else) for no\" ");
    String diceResponse = myScanner.nextLine();
    
    
    
    switch (diceResponse) {
      //User wants to manually enter the two dice numbers  
      case "Y": case "y":
        //Obtain dice #1's number
        System.out.println("Enter the roll of dice number 1: ");
        diceOne = myScanner.nextInt();

        //Obtain dice #2's number
        System.out.println("Enter the roll of dice number 2: ");
        diceTwo = myScanner.nextInt();
        break;
        
      //User wants to have two randomly generated dice  
      default:
      diceOne = (int) (Math.random() * 6 + 1);
      diceTwo = (int) (Math.random() * 6 + 1);
      //Print out the rolled dice
      System.out.println("The dice that were rolled are: " + diceOne + " and " + diceTwo); 
      break;
    }
    
    
    //Make sure both dices are in range (1-6) with a switch statement
    switch (diceOne) {
      case 1: case 2: case 3: case 4: case 5: case 6: // Dice One valid
        switch (diceTwo) {
          case 1: case 2: case 3: case 4: case 5: case 6: //Both dice valid
            break;       
          default: //Dice Two not valid - even though dice one is
            System.out.println("One of the specified dice roll's is out of range (1-6)");
            return;        
        }            
        break;        
      default: // Dice One value is invalid already
        switch (diceTwo) {
          case 1: case 2: case 3: case 4: case 5: case 6: //Dice 2 valid but dice 1 not
            System.out.println("One of the specified dice roll's is out of range (1-6)");
            return;        
          default: //Both dice invalid
            System.out.println("One of the specified dice roll's is out of range (1-6)");
            return;        
        }        
    }

    
    
    
      
      //Set diceSum variable to the sum of diceOne and diceTwo to make it easier to find the combination w/o repeating code for order of the dice rolled
      int diceSum = diceOne + diceTwo;
      
      
      //Start Testing for sum combinations
      //In the case of a sum having two or more combinations, run a nested switch statement to determine which slang term to use
      //Example. Sum of 4 has rolls of 2 and 2 OR 1 and 3
    
      switch (diceSum) {
        case 2:
          System.out.println("The slang for this roll is: Snake Eyes");
          break;
        case 3:
          System.out.println("The slang for this roll is: Ace Deuce");
          break;
        case 4:
          switch (diceOne) {
            case 2:
              System.out.println("The slang for this roll is: Hard Four");
              break;
            default:
              System.out.println("The slang for this roll is: Easy Four");
              break;
          }
          break;
        case 5:
          System.out.println("The slang for this roll is: Fever Five");
          break;
        case 6:
          switch (diceOne) {
            case 3:
              System.out.println("The slang for this roll is: Hard Six");
              break;
            default:
              System.out.println("The slang for this roll is: Easy Six");
              break;
          }          
          break;
        case 7:          
          System.out.println("The slang for this roll is: Seven Out");
          break;
        case 8:
          switch (diceOne) {
            case 4:
              System.out.println("The slang for this roll is: Hard Eight");
              break;
            default:
              System.out.println("The slang for this roll is: Easy Eight");
              break;
          }            
          break;
        case 9:
          System.out.println("The slang for this roll is: Nine");
          break;
        case 10:
          switch (diceOne) {
            case 5:
              System.out.println("The slang for this roll is: Hard Ten");
              break;
            default:
              System.out.println("The slang for this roll is: Easy Ten");
              break;
          }            
          break;
        case 11:
          System.out.println("The slang for this roll is: Yo-leven");
          break;
        case 12:
          System.out.println("The slang for this roll is: Boxcars");
          break;
          
      }
          
  
    
  }
  
}