// Jack Liu: CSE2 - Lab06
// 10/11/18

import java.util.Scanner;

public class PatternA {
  public static void main(String args[]){
    
    int input = 0;
    
    
    Scanner scanner = new Scanner( System.in ); //Setup new instance of the Scanner class called "scanner"
    
    // Confirm an integer was entered
    boolean enteredInt = true;
    while (enteredInt) {
      System.out.println("Please enter an integer between 1 and 10 ");
      if (scanner.hasNextInt() == true) {
        input = scanner.nextInt();
        if (input <= 10 && input >= 1){
           enteredInt = false;
        }
        else {
          scanner.nextLine();
        }
      }
      else{
        scanner.nextLine();
      }
    }    
    
    
    
    for(int numRows = 1; numRows <= input; numRows++){
        for(int numDigits = 1; numDigits <= numRows; numDigits++){
          System.out.print(numDigits + " ");
        }
        System.out.println();
    }
    
    
    
    
    
    
  
  }
}  