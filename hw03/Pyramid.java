//Jack Liu: CSE2 - Hw03
//9/13/18
//
//This program prompts the user for the dimensions of a pyramid and
//displays the volume inside the pyramid


import java.util.Scanner;

public class Pyramid {
  
  public static void main(String args[]){
    
    Scanner myScanner = new Scanner( System.in ); //Setup new instance of the Scanner class called "myScanner"
    
    //Retrieve the input for the square side of the pyramid
    System.out.println("The square side of the pyramid is: ");
    Double squareSide = myScanner.nextDouble();
    
    //Retrieve the input for the height of the pyramid
    System.out.println("The height of the pyramid is: ");
    Double height = myScanner.nextDouble();
    
    //Calculates the volume of the pyramid by multiplying the square side to itself and the height of the pyramid and then dividing by 3
    Double volume = ((squareSide * squareSide) * height) / 3;
    
    //Print out the volume of the pyramid
    System.out.println("The volume inside the pyramid is: " + volume);

  }
  
}