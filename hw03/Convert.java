//Jack Liu: CSE2 - Hw03
//9/13/18
//
//This program converts the qty of rain in inches and number of acres of land (convered in rain)
//to a calculated amount of rain in cubic miles

import java.util.Scanner;

public class Convert {
  
  public static void main(String args[]){
    
    Scanner myScanner = new Scanner( System.in ); //Setup new instance of the Scanner class called "myScanner"
    
    //Retrieve the input for the affected area in acres
    System.out.println("Enter the affected area in acres: ");
    Double affectedAcres = myScanner.nextDouble();
    
    //Retrieve the input for the average rainfall in inches for the affected area
    System.out.println("Enter the rainfall in the affected area: ");
    Double averageRainfall = myScanner.nextDouble();
    
    
    //Multiply acres*inches by "27154.29" to obtain gallons of rainfall(Conversion value found off google)
    Double gallonsOfRainfall = (affectedAcres * averageRainfall) * 27154.29;
    
    //Conversion value of cubic miles per gallon
    Double gallonsToCubicMilesValue = 9.082e-13;
      
    //Find resulting amount of rain in cubic miles by multiplying the gallons of rainfall by the cubic miles conversion value
    Double totalRainFall = gallonsOfRainfall * gallonsToCubicMilesValue;
     
    System.out.println(totalRainFall +  " cubic miles");
    
    
    
  }
  
  
}