//Jack Liu: CSE2 - Hw09
//Program removes element from an array (through 2 methods)

import java.util.Scanner;
import java.util.Random;
public class RemoveElements{
  
  public static void main(String [] arg){
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);

      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);

        System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);

      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    } while(answer.equals("Y") || answer.equals("y"));
   }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
  
  
  
  public static int[] randomInput(){
    int[] array = new int[10];
    for (int i = 0; i<10; i++){
      int randomNumber = (int) (Math.random() * 9);
      array[i] = randomNumber;
    }
    return array;
  }
  
  
   public static int[] delete(int[] list, int pos){
    int[] array = new int[(list.length - 1)];
    boolean foundPos = false;
    if (pos > list.length) {
      System.out.println("Out of bounds pos");
      return list;
    }
     
    else {
      for (int i = 0; i<(list.length-1); i++){
        if (i == pos){
          foundPos = true;
          array[i] = list[(i+1)];
        }

        else if (foundPos == true){
          array[i] = list[(i+1)];
        }
        else {
          array[i] = list[i];
        }
     }
    }    
    return array;
  }
  
  
   public static int[] remove(int[] list, int target){
    int[] array = new int[(list.length - 1)];
    boolean foundTarget = false;
      for (int i = 0; i<(list.length-1); i++){
        if (list[i] == target){
          foundTarget = true;
          array[i] = list[(i+1)];
        }
        
        else if (foundTarget == true){
          array[i] = list[(i+1)];
        }
        
        else {
          array[i] = list[i];
        }
     }
    if (foundTarget == false) {
      return list;
    }
    return array;
  }
  
  
  
}
