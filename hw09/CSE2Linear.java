//Jack Liu: CSE2 - Hw09
//Program utilizes binary and linear array search for elements in the array


import java.util.Scanner;
import java.util.Random;

public class CSE2Linear {
  public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      int[] numbers = new int[15];
      int counter = 0;
      while (counter < 15){
        System.out.println("Enter an integer ");

        if (scan.hasNextInt() == true){
          int integerEntered = scan.nextInt();
          if (integerEntered >= 0 && integerEntered <= 100){
            if (counter == 0){ // first entry into array
              numbers[counter] = integerEntered;
              counter++;
            }
            else { //not first entry into array so need to test if greater than previous

              if (integerEntered < numbers[(counter-1)]){
                System.out.println("Error: The integer you entered is not greater or equal to the previous one");
              }
              else {//all tests passed
                numbers[counter] = integerEntered;
                counter++;
              }

            }

          }

          else { //out of range

            System.out.println("Error: The integer you entered is out of range (0-100)");
          }


        }

        else { //not an integer

          System.out.println("Error: You did not enter an integer");
        }

        scan.nextLine();
      }

    for (int i=0; i<numbers.length; i++){ //Initial print
        System.out.print(numbers[i] + " ");
    }

    int search = 0;
    System.out.println("");
    System.out.println("Enter a grade to search for: ");
    search = scan.nextInt();


    binarySearch(numbers, search);


    numbers = scramble(numbers); //scramble the numbers

    for (int i=0; i<numbers.length; i++){ //print after scramble
        System.out.print(numbers[i] + " ");
    }

    System.out.println("");
    System.out.println("Enter a grade to search for: ");
    search = scan.nextInt();

    linearSearch(numbers, search);




  }

  public static void binarySearch(int numbers[], int searchedNumber){
    int iterations = 0;
    int searchTerm = numbers.length / 2;
    boolean found = false;

    while (iterations < (numbers.length/2)){
      iterations++;
      if (numbers[searchTerm] == searchedNumber){
        found = true;
        break;
      }
      else{
        if (searchedNumber > numbers[searchTerm]){
          searchTerm = ((numbers.length - searchTerm)/2) + searchTerm;

        }
        else{
          searchTerm = searchTerm - (searchTerm/2);
        }
      }
    }

    if (found == true){
      System.out.println(searchedNumber + " " + "was found in the list with " + iterations + " iterations");
    }
    else {
      System.out.println(searchedNumber + " " + "was not found in the list with " + iterations + " iterations");
    }

  }



  public static void linearSearch(int numbers[], int searchedNumber){
    int iterations = 0;
    boolean found = false;
    for (int i=0; i<numbers.length; i++){ //Initial print
        iterations++;
        if (numbers[i] == searchedNumber){
          found = true;
          break;
        }

    }

    if (found == true){
      System.out.println(searchedNumber + " " + "was found in the list with " + iterations + " iterations");
    }
    else {
      System.out.println(searchedNumber + " " + "was not found in the list with " + iterations + " iterations");
    }

  }

  public static int[] scramble(int numbers[]){
    int[] newArray = new int[numbers.length];
    newArray = numbers;
    for (int i = 0; i<numbers.length; i++){
      int randomNumber = (int) (Math.random() * numbers.length);
      int tempNumber = newArray[i];
      newArray[i] = newArray[randomNumber];
      newArray[randomNumber] = tempNumber;
    }

    return newArray;

  }




}
