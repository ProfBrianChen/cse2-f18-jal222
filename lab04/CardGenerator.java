//Jack Liu: CSE2 - Lab04
//9/20/18

import java.util.*;

//This program will pick a random card with a custom number and suit

public class CardGenerator {
    // main method required for every Java program
   	public static void main(String[] args) {
      
      //Generate random number between 1-52
      int randomNumber = (int) (Math.random() * 53);
      String suit = "Default"; //Initialize suit variable
      String cardName; //Define cardName variable
      String identity; //Define identity variable -> String text to be displayed to user
      
      //Assign the suit of the card based on the random the generated number falls in
      if (randomNumber <= 13){
        suit = "Diamonds";
      }
      
      else if (randomNumber >= 14 && randomNumber <= 26){
        suit = "Clubs";
      }
      
      else if (randomNumber >= 27 && randomNumber <= 39){
        suit = "Hearts";
      }
      
      else if (randomNumber >= 40 && randomNumber <= 52){
        suit = "Spades";
      }
      
      //Assign face cards to a different card name so there is no "13 of Spades"
      switch (randomNumber) {
        case 1: case 14: case 27: case 40:
          cardName = "Ace ";
          break;
        
        case 13: case 26: case 39: case 52:
          cardName = "King ";
          break;
        
        case 12: case 25: case 38: case 51:
          cardName = "Queen ";
          break;
          
        case 11: case 24: case 37: case 50:
          cardName = "Jack ";
          break;
          
        default: //The default case is if its not a face card and thus is a normal card -> Assign cardName to just the random number integer
          cardName = randomNumber + " ";
          break;
      }
      
      //Form the identity of the card
      identity = cardName + "of " + suit;
      
      //Print out the identity of the card
      System.out.println("You picked the " + identity);
      
      
    }
  
}