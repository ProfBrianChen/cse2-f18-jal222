//Jack Liu: CSE2 - Hw07
//11/5/18
//This program manipulates strings and practices writing different methods to do so


import java.util.Scanner;

public class WordTools {

  public static void main(String args[]){
    Scanner scanner = new Scanner (System.in);


    String foundText = sampleText();
    String menuChoice = "";
    do {
      menuChoice = printMenu();

      switch (menuChoice){
        case "c":
          getNumOfNonWSCharacters(foundText);
          int nonWSChar = getNumOfNonWSCharacters(foundText);
          System.out.println("Number of non-whitespace characters: " + nonWSChar);
          break;
        case "w":
          int numWords = getNumOfWords(foundText);
          System.out.println("Number of words: " + numWords);
          break;
        case "f":
          System.out.println("Enter a word of phrase to be found:");
          String searchTerm = scanner.nextLine();
          int numFoundWords = findText(foundText, searchTerm);
          System.out.println("\"" + searchTerm + "\"" + " " + "instances:" + " " + numFoundWords);
          break;
        case "r":
          String excString = replaceExclamation(foundText);
          System.out.println("Edited text: " + excString);
          break;
        case "s":
          String shortenString = shortenSpace(foundText);
          System.out.println("Edited text: " + shortenString);
          break;
        case "q":
          return;
        default:
          break;
      }

    } while (menuChoice != "q");


  }



  public static String sampleText(){
    Scanner scanner = new Scanner (System.in);
    System.out.println("Enter a sample text: ");
    System.out.println("");
    String enteredText = scanner.nextLine();
    return enteredText;

  }


  public static String printMenu(){
    Scanner scanner = new Scanner (System.in);
    String chosenOption = "";
    do {
      System.out.println("");
      System.out.println("MENU");
      System.out.println("c - Number of non-whitespace characters");
      System.out.println("w - Number of words");
      System.out.println("f - Find text");
      System.out.println("r - Replace all !'s'");
      System.out.println("s - Shorten spaces");
      System.out.println("q - Quit");
      System.out.println("");
      System.out.println("Choose an option: ");

      chosenOption = scanner.nextLine();

    } while (
            chosenOption == "c" ||
            chosenOption == "w" ||
            chosenOption == "f" ||
            chosenOption == "r" ||
            chosenOption == "s" ||
            chosenOption == "q"
            );
    return chosenOption;
  }

  public static int getNumOfNonWSCharacters(String string){
    int numNonWSChar = 0;
    int stringLength = string.length();

    for (int i = 0; i < stringLength; i++){
      char charFound = string.charAt(i);
      if (charFound != ' '){
        numNonWSChar++;
      }
    }

    return numNonWSChar;


  }


  public static int getNumOfWords(String string){
    int numWords = 0;
    int stringLength = string.length();

    for (int i = 0; i < stringLength; i++){
      char charFound = string.charAt(i);
      if (charFound == ' '){
        numWords++;
      }
    }
    numWords++;
    return numWords;
  }


  public static int findText(String string, String findTerm){
    int stringLength = string.length();
    String instancedWord = "";
    int foundTerms = 0;

    for (int i = 0; i < stringLength; i++){
      char charFound = string.charAt(i);
      if (charFound == ' ' || charFound == '.'){ //Word found
        if (instancedWord.equals(findTerm)){ //Word matches input
          foundTerms++;
        }
        instancedWord = "";
      }
      else {
        instancedWord = instancedWord + charFound;
      }
    }
    return foundTerms;

  }

  public static String replaceExclamation(String string){
    int stringLength = string.length();
    String returnString = "";

    for (int i = 0; i < stringLength; i++){
      char charFound = string.charAt(i);
      if (charFound == '!'){ //Replace ! with .
        returnString = returnString + ".";
      }
      else {
        returnString = returnString + charFound;
      }
    }
    return returnString;
  }

  public static String shortenSpace(String string){
    int stringLength = string.length();
    String returnString = "";

    boolean emptyBefore = false;
    for (int i = 0; i < stringLength; i++){
      char charFound = string.charAt(i);
      if (charFound == ' '){ // Space found to be empty
        if (emptyBefore == true){ //Space before was empty as well

        }
        else {
          returnString = returnString + " "; //Wasn't an empty space before
          emptyBefore = true;
        }

      }
      else {
        emptyBefore = false;
        returnString = returnString + charFound;
      }
    }
    return returnString;
  }

}
