//Jack Liu: CSE2 - Hw06
//10/21/18
//This program will print out an encrypted X (through stars). The amount of stars is dependent
//on user entry


import java.util.Scanner;

public class EncryptedX {
  public static void main(String args[]){
    
    Scanner scanner = new Scanner( System.in );
    int input = -1; //Initialize at -1 => will check if between 0-100 later
    
    boolean enteredInt = true;
    
    while (enteredInt) {
      System.out.println("Enter an integer between 1 and 100");
      
      if (scanner.hasNextInt() == true){
        input = scanner.nextInt();
        if (input >= 0 && input <= 100){
          enteredInt = false;
        }
        else {
          scanner.nextLine();
        }
      }
      else {
        scanner.nextLine();
      }
    }
    
    
    
    for (int numRows = 0; numRows < input; ++numRows){ //Prints rows -> start at row 1, end at input
      
      int symSpace = -1; //This will keep track of which corresponding, symmetrical space will be empty as well
      
      for (int numStars = 0; numStars < input; ++numStars) { //Prints stars in row -> start at star 1, end when number of stars = input
        
        int confirm = 0;
        if (numRows * 2 <= input){
          confirm = numStars - 1;
        }
        else {
          confirm = numStars;
        }
        
        
        if (numStars == numRows){
          System.out.print(" ");

          if (input / 2 != numStars){ //Make sure that we are not at the center space of the star
            symSpace = input - (numStars - 1);
          }

        }
       
        
        
        else {
//           if (numStars == symSpace){
//             System.out.print("b");
//           }
          
          if (numRows == input - numStars){
            System.out.print(" ");
          }
          
          System.out.print("*");
        }
        
        

      }
      System.out.println(""); //Prints out a new line   

      
      
    }
    

  }  
}  