public class WelcomeClass{
  
  public static void main(String args[]){
    
    //Prints the Welcome Sign - Copied from the hw-01 google docs example
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
    
    //Prints my lehigh ID in a fancy way
    System.out.println("^  ^  ^  ^  ^  ^");
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-J--A--L--2--2--2->");
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("v  v  v  v  v  v");
                       
    //Prints my tweet-length autobiographic statement              
    System.out.println("Hi, my name is Jack and I'm from Ann Arbor, Michigan");
    
  }

}