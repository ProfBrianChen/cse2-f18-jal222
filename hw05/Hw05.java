//Jack Liu: CSE2 - Hw05
//10/8/18
//This program will randomly draw a specified amount of hands of five cards. Poker hand match probabilities will be calculated
//based on how many matches are drawn over how many hands were drawn.


import java.util.Scanner;

public class Hw05 {
  public static void main(String args[]){

    //Initialize counters of different matches
    int fourOfAKind = 0;
    int threeOfAKind = 0;
    int twoPair = 0;
    int pair = 0;


    Scanner scanner = new Scanner( System.in ); //Setup new instance of the Scanner class called "scanner"

    int numHands = 0; //variable for number of hands generated
    //Define card values -> These will be reset in the while loop for every new hand generated
    int card1 = 0;
    int card2 = 0;
    int card3 = 0;
    int card4 = 0;
    int card5 = 0;

    // Confirm an integer was entered
    boolean enteredInt = true;
    while (enteredInt) {
      System.out.println("Please enter the number of times you would like to generate hands ");
      if (scanner.hasNextInt() == true){
        numHands = scanner.nextInt();
        enteredInt = false;
      }
      else{
        scanner.nextLine();
      }
    }



    int numHandsCounter = 0; // Run loops until counter reachs amount of times requested to generate by hand
    while (numHandsCounter < numHands) {

      int fiveCardCounter = 1; //Counter for drawing five cards
      while (fiveCardCounter <= 5){

        //Generate random number between 1-52
        int randomNumber = (int) (Math.random() * 53);

        //Check for duplicates -> and reroll until duplicate is removed
        while (randomNumber == card1 || randomNumber == card2 || randomNumber == card3 || randomNumber == card4 || randomNumber == card5) {
          randomNumber = (int) (Math.random() * 53);
        }


       int amountOfThirteen; //Variable that finds how how many factors of thirteen fit into the random number: Ex. 46 has 2 factors of 13
       //Checks if the randomNumber is a multiple of 13 -> If it is, subtract 1 from the "amountOfThirteen" value (ex. 26 would assign 1 to amountOfThirteen)
       if (randomNumber % 13 == 0){
         amountOfThirteen = (randomNumber / 13) - 1;
       }
       else {
         amountOfThirteen = randomNumber / 13;
       }

       //Now assign card values for the 5 cards(Between 1-13 -> Each value represents a type of card..disregarding suit)
       switch (fiveCardCounter) {
          case 1:
            if (amountOfThirteen == 0){ //Value is already 13 or below -> so just assign the value to card
              card1 = randomNumber;
            }
            else{ //Value is above 13 so subtract the amount of 13's that fit in the number
              card1 = (randomNumber - (amountOfThirteen * 13));
            }
            break;
          case 2:
            if (amountOfThirteen == 0){ //Value is already 13 or below -> so just assign the value to card
              card2 = randomNumber;
            }
            else{ //Value is above 13 so subtract the amount of 13's that fit in the number
              card2 = (randomNumber - (amountOfThirteen * 13));
            }
            break;
          case 3:
            if (amountOfThirteen == 0){ //Value is already 13 or below -> so just assign the value to card
              card3 = randomNumber;
            }
            else{ //Value is above 13 so subtract the amount of 13's that fit in the number
              card3 = (randomNumber - (amountOfThirteen * 13));
            }
            break;
          case 4:
            if (amountOfThirteen == 0){ //Value is already 13 or below -> so just assign the value to card
              card4 = randomNumber;
            }
            else{ //Value is above 13 so subtract the amount of 13's that fit in the number
              card4 = (randomNumber - (amountOfThirteen * 13));
            }
            break;
          case 5:
            if (amountOfThirteen == 0){ //Value is already 13 or below -> so just assign the value to card
              card5 = randomNumber;
            }
            else{ //Value is above 13 so subtract the amount of 13's that fit in the number
              card5 = (randomNumber - (amountOfThirteen * 13));
            }
            break;
          default:
            System.out.println("Error");
            break;
       }
       fiveCardCounter++;
     }

       int findMatchCounter = 1;
       int potentialPairs = 0;
       //while loop for all 13 possible card values
       while (findMatchCounter <= 13){

         int matches = 0;
         int testCardMatch = 1;

         while (testCardMatch <= 5){
          switch (testCardMatch) {
            case 1:
              if (card1 == findMatchCounter){
                matches++;
              }
              break;
            case 2:
              if (card2 == findMatchCounter){
                matches++;
              }
              break;
            case 3:
              if (card3 == findMatchCounter){
                matches++;
              }
              break;
            case 4:
              if (card4 == findMatchCounter){
                matches++;
              }
              break;
            case 5:
              if (card5 == findMatchCounter){
                matches++;
              }
              break;
          }


          testCardMatch++;

        }

        //Add to counters of poker hands based on matches
        if (matches == 4){
          fourOfAKind++;
        }
        else if (matches == 3){
          threeOfAKind++;
        }
        else if (matches == 2){
          potentialPairs++;
        }


         findMatchCounter++;
       }

       //Confirm if the potential pairs is a two pair or single pair
       if (potentialPairs == 2){
         twoPair++;
       }
       else if (potentialPairs == 1){
         pair++;
       }



      numHandsCounter++;
    }



    //Print out the statements
    System.out.println("The number of loops: " + numHands);
    System.out.printf("The probability of Four-of-a-kind: %.3f %n", (double) fourOfAKind / numHands);
    System.out.printf("The probability of Four-of-a-kind: %.3f %n", (double) threeOfAKind / numHands);
    System.out.printf("The probability of Four-of-a-kind: %.3f %n", (double) twoPair / numHands);
    System.out.printf("The probability of Four-of-a-kind: %.3f %n", (double) pair / numHands);

  }
}
