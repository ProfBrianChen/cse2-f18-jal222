//Jack Liu: CSE2 - Lab02
//9/6/18
//

public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
    
    //Our input data
    int secsTrip1=480;  //Number of seconds for trip 1
    int secsTrip2=3220;  //Number of seconds for trip 2
		int countsTrip1=1561;  //Number of rotations for trip 1
		int countsTrip2=9037; //Number of rotations for trip 2
      
      
    // our intermediate variables and output data. Document!
    double wheelDiameter=27.0,  //Diameter of the wheel
  	PI=3.14159, //Standard pi value for calculation
  	feetPerMile=5280,  //Standard conversion feet per mile
  	inchesPerFoot=12,   //Standard conversion inches per foot
  	secondsPerMinute=60;  //Standard conversion seconds per minute
	  double distanceTrip1, distanceTrip2, totalDistance;  // Declaration of distance variables - which are computed later
      
      
    //Print out how long each trip took in minutes and print out the counts/rotations of each trip  
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had " + countsTrip1+" counts.");
	  System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts."); 
      
      
    //Calculate the distance of trip 1 in inches by multiplying trip 1's count by the wheel diameter and the pi
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance of trip 1 in miles
      
    //Calculate the distance of trip 2 in inches by multiplying trip 1's count by the wheel diameter and the pi      
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2; // Gives distance of trip 2 in miles
    
      
    //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
  

	}  //end of main method   
} //end of class