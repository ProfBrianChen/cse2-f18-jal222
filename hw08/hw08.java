//Jack Liu: CSE2 - Hw08
//11/13/18
//This program prints out a deck of cards, shuffles them, and draws a hand from them.

import java.util.Scanner;
public class hw08{
  public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
       //suits club, heart, spade or diamond
      String[] suitNames={"C","H","S","D"};
      String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"};
      String[] cards = new String[52];
      String[] hand = new String[5];
      int numCards = 5;
      int again = 1;
      int index = 51;
      for (int i=0; i<52; i++){
        cards[i]=rankNames[i%13]+suitNames[i/13];
        System.out.print(cards[i]+" ");
      }
      System.out.println();
      shuffle(cards);
      printArray(cards);
      System.out.println("");
      System.out.println("Hand");
      while(again == 1){
        if (index <= numCards){
          for (int i=0; i<52; i++){
            cards[i]=rankNames[i%13]+suitNames[i/13];
          }
          index = 51;
        }
         hand = getHand(cards,index,numCards);
         printArray(hand);
         index = (index - numCards);
         System.out.println("Enter a 1 if you want another hand drawn");
         again = scan.nextInt();
      }
  }

  public static void printArray(String cards[]){
    for (int i=0; i<(cards.length); i++){
      System.out.print(cards[i] + " ");
    }
    System.out.println("");
  }

  public static void shuffle(String cards[]){
    System.out.println("");
    System.out.println("Shuffled");
    for (int i=1; i<=100; i++){ //shuffle 100 times
      int randomNumber = (int) (Math.random() * 52);
      String shuffledCard = cards[randomNumber];
      cards[randomNumber] = cards[0];
      cards[0] = shuffledCard;
    }
  }

  public static String[] getHand(String cards[], int index, int numCards){
    int cardToBeTaken = index;
    String[] hand = new String[numCards];
    for (int i=0; i<numCards; i++){
      String foundCard = cards[cardToBeTaken];
      hand[i] = foundCard;
      cardToBeTaken = cardToBeTaken - 1;
    }

    return hand;

  }



}
