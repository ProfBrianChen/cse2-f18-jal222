//Jack Liu: CSE2 - Hw02
//9/6/18
//
//Arithmetic Calculations -> Computes the cost of items bought in a shopping store while considering tax

public class Arithmetic{
  
  public static void main(String args[]){
    
    //Assumptions / Input variables -> Given by assignment
    int numPants = 3; //Number of pairs of pants
    double pantsPrice = 34.98; //Cost per pair of pants

    int numShirts = 2; //Number of sweatshirts
    double shirtPrice = 24.99; //Cost per shirt

    int numBelts = 1; //Number of belts
    double beltCost = 33.99; //cost per belt

    double paSalesTax = 0.06; //the tax rate

    
    //Calculations based on input data
    double pantsTotalCost; //total cost of pants
    double shirtsTotalCost; //total cost of shirts
    double beltsTotalCost; //total cost of belts
    
    double salesTaxPants; //Sales tax charged on pants
    double salesTaxShirts; //Sales tax charged on shirts
    double salesTaxBelts; //Sales tax charged on belts
    
    double totalPurchaseCostPriorTax; 
    double totalSalesTax; // Calculate the total sales tax
    double totalPaid; //Total amount paid for the entire transaction including tax
    
    pantsTotalCost = numPants * pantsPrice; //Multiply number of pants by price per pants to find total cost of pants
    shirtsTotalCost = numShirts * shirtPrice; //Multiply number of shirts by price per shirts to find total cost of shirts
    beltsTotalCost = numBelts * beltCost; //Multiply number of belts by price per belts to find total cost of belts
    
    //To find the sales tax per item: Multiply the total cost of the item by the paSalesTax variable
    salesTaxPants = paSalesTax * pantsTotalCost;
    salesTaxShirts = paSalesTax * shirtsTotalCost;
    salesTaxBelts = paSalesTax * beltsTotalCost;
    
    
    totalPurchaseCostPriorTax = pantsTotalCost + shirtsTotalCost + beltsTotalCost; //Total cost of all the items prior to tax
    totalSalesTax = salesTaxBelts + salesTaxShirts + salesTaxPants; //Adding all the individual sales tax to find the total sales tax
    
    totalPaid = totalPurchaseCostPriorTax + totalSalesTax; // Find the total amount paid by adding the total cost of all items prior to tax to the total sales tax
    
    
    //Display the output by printing the costs -> Includes rounding function of math to make the printed line not have extra decimals
    System.out.println("The total cost of the pants is $" + Math.round(pantsTotalCost * 100)  / 100.00d);
    System.out.println("The total cost of the shirts is $" + Math.round(shirtsTotalCost * 100)  / 100.00d);
    System.out.println("The total cost of the belts is $" + Math.round(beltsTotalCost * 100)  / 100.00d);
    
    System.out.println("The sales tax for the pants is $" + Math.round(salesTaxPants * 100)  / 100.00d);
    System.out.println("The sales tax for the shirts is $" + Math.round(salesTaxShirts * 100)  / 100.00d);
    System.out.println("The sales tax for the belts is $" + Math.round(salesTaxBelts * 100)  / 100.00d);
    
    System.out.println("The total cost of purchases before tax is $" + Math.round(totalPurchaseCostPriorTax * 100)  / 100.00d);
    System.out.println("The total sales tax is $" + Math.round(totalSalesTax * 100)  / 100.00d);
    System.out.println("The total cost of the purchases including tax is $" + Math.round(totalPaid * 100)  / 100.00d);
    
    
      
  }

}