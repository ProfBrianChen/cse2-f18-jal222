//Jack Liu: CSE2 - Lab03
//9/13/18


import java.util.Scanner;

//This program will split a bill evenly after considering tax between a inputed about of people by displaying
//the amount of dollars, dimes, and pennies owed

public class Check{
    // main method required for every Java program
   	public static void main(String[] args) {
          Scanner myScanner = new Scanner( System.in ); //new instance of scanner
      
          //Retrieve the input for the cost prior to tip
          System.out.print("Enter the original cost of the check in the form xx.xx: ");
          double checkCost = myScanner.nextDouble();
      
          //Retrieve the input for the tip paid on the bill
          System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );
          double tipPercent = myScanner.nextDouble();
          tipPercent = tipPercent / 100; //We want to convert the percentage into a decimal value

          //Retrieve the input for the amount of people that went to dinner / number of people to split the bill with  
          System.out.print("Enter the number of people who went out to dinner: ");
          int numPeople = myScanner.nextInt();

          double totalCost; //Declaration of total cost variable
          double costPerPerson; //Declaration of cost per person variable
          int dollars, dimes, pennies; //Declaration of variables for dollars, dimes, and pennies
          totalCost = checkCost * (1 + tipPercent); //Calculate the total cost by taking the check cost and adding the tip
          costPerPerson = totalCost / numPeople; //Calculate the cost per person by dividing the total cost by number of people
      
          //get the whole amount, dropping decimal fraction
          dollars=(int)costPerPerson;
          //get dimes amount, e.g., 
          // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
          //  where the % (mod) operator returns the remainder
          //  after the division:   583%100 -> 83, 27%5 -> 2 
          dimes=(int)(costPerPerson * 10) % 10;
          pennies=(int)(costPerPerson * 100) % 10;
      
          //Print out how much each person owes in dollars, dimes, and pennies
          System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);


    }  //end of main method   
} //end of class